FROM alpine:latest
COPY hello-world hello-world
ENTRYPOINT ["/bin/sh", "hello-world"]

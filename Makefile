### Set this to your gitlab user

REGISTRY_ACCESS_USER ?= jarv

chk_token:
ifndef REGISTRY_ACCESS_TOKEN
	$(error Please export REGISTRY_ACCESS_TOKEN in your environment)
endif


login-staging:
	@docker login -u ${REGISTRY_ACCESS_USER} -p ${REGISTRY_ACCESS_TOKEN} registry.staging.gitlab.com
login-prod:
	@docker login -u ${REGISTRY_ACCESS_USER} -p ${REGISTRY_ACCESS_TOKEN} registry.gitlab.com
login-pre:
	@docker login -u ${REGISTRY_ACCESS_USER} -p ${REGISTRY_ACCESS_TOKEN} registry.pre.gitlab.com

build-staging:
	docker build -t registry.staging.gitlab.com/jarv/registry-test -f Dockerfile .
pull-staging:
	docker pull registry.staging.gitlab.com/${REGISTRY_ACCESS_USER}/registry-test
push-staging:
	docker push registry.staging.gitlab.com/${REGISTRY_ACCESS_USER}/registry-test	

build-prod:
	docker build -t registry.gitlab.com/jarv/registry-test -f Dockerfile .
pull-prod:
	docker pull registry.gitlab.com/${REGISTRY_ACCESS_USER}/registry-test
push-prod:
	docker push registry.gitlab.com/${REGISTRY_ACCESS_USER}/registry-test	

build-pre:
	docker build -t registry.pre.gitlab.com/jarv/registry-test -f Dockerfile .
pull-pre:
	docker pull registry.pre.gitlab.com/${REGISTRY_ACCESS_USER}/registry-test
push-pre:
	docker push registry.pre.gitlab.com/${REGISTRY_ACCESS_USER}/registry-test	

check-staging: chk_token login-staging build-staging push-staging pull-staging
check-production: chk_token login-prod build-prod push-prod pull-prod
check-pre: chk_token login-pre build-pre push-pre pull-pre
